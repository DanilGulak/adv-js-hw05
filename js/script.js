const btn = document.querySelector('#btn')
const infoContainer = document.createElement('div')

btn.addEventListener('click', () => {

    async function getUserIp() {

        const getIp = await fetch('https://api.ipify.org/?format=json', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json'
            }
        })

        return getIp.json()
    }

    getUserIp().then(d => {

        async function getUserInfo() {

            const getInfo = await fetch(`http://ip-api.com/json/${d.ip}?fields=status,message,continent,country,countryCode,region,regionName,city,district,zip,lat,lon,timezone,isp,org,as,query`, {
                method: 'GET',
                headers: {
                    'Content-type': 'application/json'
                }
            })
            return getInfo.json()
        }

        getUserInfo().then(d => {
            document.body.append(infoContainer)

            infoContainer.insertAdjacentHTML('beforeend', `<p>Continent: ${d.continent}</p> <p>Country: ${d.country}</p><p>Region: ${d.regionName}</p><p>City: ${d.city}</p><p>District: ${d.district = 'No information'}</p>`)
        })
    })
})